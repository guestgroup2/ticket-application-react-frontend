# ticket-application-react-frontend

## Scenario

For a journey, a passenger can carry up to 5 items of luggage. Clerks need to register the weight of
each one and each piece of baggage is assigned a code. After entering all items, a fee for the
baggage is calculated as follows:
1. 1 small item up to 7kg is free
2. 1 big item up to 25kg is free
3. Fee for an item weighing 0-7kg is 10 euro
4. Fee for an item weighing 7-25kg is 25 euro
5. Fee for overweight item is 5 euro per kg.
It is possible to add overweight fee for free items if it is advantageous for the passenger, i.e. a pair of
9kg and 15kg items would result in a fee of 10 euro (2kg overweight on top of the 7kg free item)
Sample application should offer 5 fixed tickets (5 passengers to check in), we don’t need any edit
functionality for these.
After selecting a ticket, a baggage check screen should be presented where the clerk can type in
baggage weights.
After confirming baggage weights, the application either automatically chooses the lowest possible
fee, or gives options to the clerk to categorize type of luggage (free, free + overweight, light, heavy,
…) and then computes the fee. Conditions for each type need to be verified.
Once the fee is determined, the application displays an overview of luggage IDs, their weight, and
respective fee, as well as total.

Baggage Category:
1. Label: Free, Value: FREE, Description: It represents the free of charge of baggage.
2. Label: Overweight, Value: FREE_OVER, Description: It represents the free part of 0-7kg or up 25kg free + overweight part.
3. Label: Light, Value: LIGHT, Description: It represents the baggage that weight is between 0 and 7kg.
4. Label: Heavy, Value: HEAVY, Description: It represents the baggage that weight is between 7kg and 25kg.
5. Label: Extreme Heavy, Value: EXT, Description: It represents the baggage that weight is larger than 25KG.

## Frond-End Application Structure/Architecture

1.	ReactJS
2.	Mui MaterialUI as UI component: button & the Data Grid
3.	AXIOS Utility as the RESTful call from the back-end system.
4.	Structure:
[Structure Screen](https://gitlab.com/guestgroup2/ticket-application-react-frontend/-/blob/main/frontend-structure.png)
    1. package.json: contains the dependency of packages.
    2. src/App.js: the core of the application that describes the UI & UI interactions.


## Frond-End Application Installation and Run

1.	Install nodeJS
2.	Install NPM
3.	Install Visual Studio Code
4.	On terminal:

    1.	Under the project folder:

        1.	Commands: 

            1.	npm install
            2.	npm start
            3.	After the application is started, open the browser and enter http://localhost:3000 to use the application.
            4.	Operation Steps Screen:
                1. [Step1](https://gitlab.com/guestgroup2/ticket-application-react-frontend/-/blob/main/front-end-use-application1.png)
                2. [Step2](https://gitlab.com/guestgroup2/ticket-application-react-frontend/-/blob/main/front-end-use-application2.png)
                3. [Step3](https://gitlab.com/guestgroup2/ticket-application-react-frontend/-/blob/main/front-end-use-application3.png)
                4. [Step4](https://gitlab.com/guestgroup2/ticket-application-react-frontend/-/blob/main/front-end-use-application4.png)
                5. [Step5](https://gitlab.com/guestgroup2/ticket-application-react-frontend/-/blob/main/front-end-use-application5.png)





