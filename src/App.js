import { useState } from "react";
import "./styles.css";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Unstable_Grid2';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';


const SimpleForm = () => {
  const rows = [
    { id: 1, ticketNo: 'A222', passenger: 'John', original: 'HKG', destination: 'SYD', itemId: 'A123', weight: '6.0', category: 'FREE', charge: '0.0', clerk: 'Mary', amount: 0.0 },
  ];
  
  const columns = [
    { field: 'ticketNo', headerName: 'TIcket ID', width: 120 },
    { field: 'passenger', headerName: 'Passenger', width: 120 },
    { field: 'original', headerName: 'Original', width: 120 },
    { field: 'destination', headerName: 'Destination', width: 150 },
    { field: 'itemNo', headerName: 'Luggage', width: 120 },
    { field: 'weight', headerName: 'Weight', width: 100, editable: true },
    { field: 'category', type: 'singleSelect', headerName: 'Category', width: 150, valueOptions: [
      {value: 'FREE', label: 'Free'},
      {value: 'FREE_OVER', label: 'Over Weight'},
      {value: 'LIGHT', label: 'Light'},
      {value: 'HEAVY', label: 'Heavy'},
      {value: 'EXT', label: 'Extreme Heavy'}
    ], editable: true },
    { field: 'charge', headerName: 'Charge', width: 80 },
    { field: 'clerk', headerName: 'Clerk', width: 150 },
    { field: 'amount', headerName: 'Ticket Amount', width: 150 },
  ];

  const [ticket, setTicket] = useState({});

  const generateTicket = async () => {
    let s = await axios.get('http://localhost:8087/generate', {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }
    });
    console.log(s.data);
    setTicket(s.data);
  }

  const mySaveOnServerFunction = (newRow) => {
    let g = JSON.parse(JSON.stringify(ticket));
    console.log("grid" + JSON.stringify(g));
    console.log("row: " + JSON.stringify(newRow));
    for (let i = 0; i < g.length; i++) {
      if (g[i].ticketId === newRow.ticketId && g[i].itemId === newRow.itemId) {
        g[i] = JSON.parse(JSON.stringify(newRow));
        setTicket(g);
        break;
      }
    }
  }

  const handleProcessRowUpdateError = (error) => {
    console.log(error.message);
  };

  const sendWeight = async () => {
    for (let i = 0; i < ticket.length; i++) {
      if (ticket[i].weight === null || ticket[i].weight === '' || ticket[i].weight === "" || ticket[i].weight === undefined) {
        alert('The weight of luggage (' + ticket[i].itemNo + ') is required.')
        return;
      }
    }
    let s = await axios.post('http://localhost:8087/min', JSON.parse(JSON.stringify(ticket)), {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }
    });
    console.log(s.data);
    setTicket(s.data);
  }

  const sendCategory = async () => {
    for (let i = 0; i < ticket.length; i++) {
      if (ticket[i].category === null || ticket[i].category === '' || ticket[i].category === "" || ticket[i].category === undefined) {
        alert('The category of luggage (' + ticket[i].itemNo + ') is required.')
        return;
      }
    }
    let s = await axios.post('http://localhost:8087/minCategory', JSON.parse(JSON.stringify(ticket)), {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }
    });
    console.log(s.data);
    if (s.data.success) {
      setTicket(JSON.parse(s.data.data));
    }
  }

  return (
    <div>
      <Grid container rowSpacing={3}>
        <Grid item xs={30}>
          <Button variant="contained" onClick={generateTicket}>Generate</Button><label>     </label>
          <Button variant="contained" onClick={sendWeight}>Calculate Minimun Amount</Button><label>     </label>   
          <Button variant="contained" onClick={sendCategory}>Calculate Amount by Category</Button>
        </Grid>
        <Grid item xs={30}>
          <DataGrid rows={ticket} columns={columns} processRowUpdate={(updatedRow, originalRow) =>{
            mySaveOnServerFunction(updatedRow); return updatedRow;
          }} onProcessRowUpdateError={handleProcessRowUpdateError} />
        </Grid>
      </Grid>
    </div>
  );
};

export default SimpleForm;
